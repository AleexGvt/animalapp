<style>
.blogShort{ border-bottom:1px solid #ddd;}
.add{background: #333; padding: 10%; height: 300px;}

.btn-blog {
    color: #ffffff;
    background-color: #37d980;
    border-color: #37d980;
    border-radius:0;
    margin-bottom:10px
}
.btn-blog:hover,
.btn-blog:focus,
.btn-blog:active,
.btn-blog.active,
.open .dropdown-toggle.btn-blog {
    color: white;
    background-color:#34ca78;
    border-color: #34ca78;
}
article h2{color:#333333;}
h2{color:#34ca78;}
 .margin10{margin-bottom:10px; margin-right:10px;}

 .map{
    height: 400px; 
    width: 100%;
 }

</style>


@extends('layouts.plantilla')

@section('cabecera')

@section('cuerpo')


@foreach ($animales as $animal)
    
    <div class="media">  
    <img src="{{$animal->ruta_img}}" alt="post img" class="align-self-start img-thumbnail mr-8" width="300" height="300">
            <div class="media-body">     
            <h1 class="mt-0">{{$animal->nombre}}</h1>
            <a>Estado: {{$animal->status}}</a>
            <br>
            <a>Sonido:</a>
            <br>
            <audio id="sound" src="{{$animal->ruta_sonido}}" preload="auto" controls></audio> 
            <article><p>
                        {{$animal->descripcion}}
                </p>
            <h3>Habitat</h3>
            <img src="{{$animal->ruta_habitat}}" alt="post img" class="align-self-start img-thumbnail mr-8" width="500" height="500">
            </p>{{$animal->habitat}}</p>   

            </article>    

                    
                
        </div>
       

@endforeach

@endsection

@endsection