

<style>
    .card{
      
    }

    .inner{
      width: 50%;
    }


</style>


@extends('layouts.plantilla')

@section('cabecera')
    
@endsection

@section('cuerpo')

@foreach ($preguntas as $pregunta)
      <br>
     <form>
      <div class="card mx-auto" style="width: 50rem;">
          <h4 class="card-title"> {{$pregunta->pregunta}} </h4>
        <div class="card-body">
            <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios1" value="{{$pregunta->res_c}}" checked=""> {{$pregunta->res_c}}
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios2" value="option"> {{$pregunta->res_ia}}
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios3" value="option"> {{$pregunta->res_ib}}
                </label>
              </div>
              <div class="radio">
                <label>
                  <input type="radio" name="optionsRadios" id="optionsRadios4" value="option"> {{$pregunta->res_ic}}
                </label>
            </div>
          </div>
        </div>

      </div>
    </form>
      <br>
      @endforeach
      <div id="inner"">
        <a class=" btn btn-primary " id=" boton" role=" button" onclick="checar();">Confirmar</a>
    </div>


    <script type="text/javascript">
      function checar() {
        var memo = document.getElementsByName('optionsRadios');
        var tmp = true;
        for (i = 0; i < memo.length; i++) {
          console.log(memo[i].checked + " " + memo[i].value);
          if (memo[i].checked && memo[i].value == "option") {
            alert("Te has equivocado, revisa tus respuetas");
            tmp = false;
            break;
          }
        }
        if (tmp) {
          window.location.replace("{{ action('Controlador@desbloquear') }}")
        }
      }
    </script>
    

      


@endsection