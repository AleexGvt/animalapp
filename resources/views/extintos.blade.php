<style>


    .row {
        text-align: center;
        padding-right: 10%;
        padding-left: 10%;
        margin:auto
    }
img.d-block.w-100{
    height: inherit;
}


.carousel-item{
    height: 500px;
}



</style>


@extends("layouts.plantilla")

@section("cabecera")

<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="https://tuswallpapersgratis.com/wp-content/plugins/download-wallpaper-resized/wallpaper.php?x=3840&y=2160&file=https://tuswallpapersgratis.com/wp-content/uploads/2014/03/Woolly-Mammoth_1024x768-787882.jpeg" class="d-block w-100">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
    <div class="carousel-item">
      <img src="https://2.bp.blogspot.com/-pB-96t5m_CA/VdqOtEi6aPI/AAAAAAAAAOk/KN7EmWJiBm0/s1600/tigre-dientes-sable-recreacion.jpg" class="d-block w-100" alt="Max-width 100%">
      <div class="carousel-caption d-none d-md-block">
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


@endsection



@section("cuerpo")

<br>
<br>
<br>
<div class="row">
    
  @foreach ($animales as $animal)
    <div class="col-lg-4">
        <img class="bd-placeholder-img rounded-circle" width="140" height="140" src="{{$animal->ruta_img}}">
        <rect width="100%" height="100%" fill="#777"/><text x="50%" y="50%" fill="#777" dy=".3em"></text></svg>
      <h2>{{$animal->nombre}}</h2>
      <p>{{$animal->descripcion}}</p>
      <p><a class="btn btn-secondary" href="{{ action('Controlador@show', $animal->id) }}" role="button">View details &raquo;</a></p>
    </div><!-- /.col-lg-4 -->
    
  
    @endforeach
  </div>

  @endsection

@section("pie")


@endsection